import os
import cv2
import matplotlib.pyplot as plt
from skimage.data import data_dir
from skimage.util import img_as_ubyte
from skimage.morphology import dilation, erosion
from skimage.morphology import disk, rectangle, diamond, star
from skimage import io


# 讀取圖片成灰度圖
img = cv2.imread('image/6-1/1.jpg',0)

#中值滤波降噪
img = cv2.medianBlur(img,5)

# 做Canny邊緣偵測
canny = cv2.Canny(img, 50, 150)

pic = io.imsave('image/Texture1/canny image.jpg',canny)
# cv2.imshow('canny',canny)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# 讀取Canny圖
# orig_phantom = img_as_ubyte(io.imread('./image/Texture1/Canny image.jpg',
#                                       as_gray=True))

# 讀取膨脹圖
orig_phantom = img_as_ubyte(io.imread('./image/Texture1/dilated.jpg',
                                      as_gray=True))

# 讀取膨脹圖2
# orig_phantom = img_as_ubyte(io.imread('./image/Texture1/dilated2.jpg',
#                                       as_gray=True))

# 讀取侵蝕圖
# orig_phantom = img_as_ubyte(io.imread('./image/Texture1/eroded.jpg',
#                                       as_gray=True))

# 顯示範例圖片
# fig, ax = plt.subplots()
# ax.imshow(orig_phantom, cmap=plt.cm.gray)
# plt.show()


# 顯示比較圖
def plot_comparison(original, filtered, filter_name):

    fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(8, 4), sharex=True,
                                   sharey=True)
    ax1.imshow(original, cmap=plt.cm.gray)
    ax1.set_title('original')
    ax1.axis('off')
    ax2.imshow(filtered, cmap=plt.cm.gray)
    ax2.set_title(filter_name)
    ax2.axis('off')
    plt.show()

# 扁平盤狀
# selem = disk(2)

# 扁平矩形形狀
selem = rectangle(7,7)

# 菱形形狀
# selem = diamond(5)

# 星型形狀
# selem = star(1)

print(selem)

# 儲存成膨脹圖
# dilated = dilation(orig_phantom, selem)
# pic = io.imsave('image/Texture1/dilated.jpg',dilated)
# plot_comparison(orig_phantom, dilated, 'dilation')

# 儲存成膨脹圖2
# dilated = dilation(orig_phantom, selem)
# pic = io.imsave('image/Texture1/dilated2.jpg',dilated)
# plot_comparison(orig_phantom, dilated, 'dilation')

# 儲存成侵蝕圖
eroded = erosion(orig_phantom, selem)
pic = io.imsave('image/Texture1/eroded.jpg',eroded)
plot_comparison(orig_phantom, eroded, 'eroded')

# 儲存成侵蝕圖2
# eroded = erosion(orig_phantom, selem)
# pic = io.imsave('image/Texture1/eroded_2.jpg',eroded)
# plot_comparison(orig_phantom, eroded, 'erosion')

# 儲存成侵蝕圖3
# eroded = erosion(orig_phantom, selem)
# pic = io.imsave('image/Texture1/eroded_3.jpg',eroded)
# plot_comparison(orig_phantom, eroded, 'erosion')