import pywt as pt
import scipy as sp
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from skimage import io, color, filters

# 讀取範例圖片
# img = pt.data.camera()

# 讀取圖片成灰度圖
img = cv2.imread('image/6-1/2.jpg',0)

# 做Canny邊緣偵測
canny = cv2.Canny(img, 50, 150)

# 使用opencv套件把圖片二值化
ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

# 使用opencv套件做閉運算
# kernel = np.ones((3,3),np.uint8)
# kernal = np.array([[0,1,0],[1,1,1],[0,1,0]],np.uint8)
kernel = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
closing = cv2.morphologyEx(thresh1, cv2.MORPH_CLOSE, kernel)

# 做膨脹
dilated = cv2.dilate(thresh1,kernel)

# 使用skimage套件把圖片轉為灰度圖
# img = color.rgb2grey(img)

# 使用skimage套件把圖片二值化
# 定一個閾值
# thresh = filters.threshold_otsu(img)
# print(thresh)
# dst =(img <= 0.5)*1.0
# print(dst)

# 使用pywt套件進行小波轉換
# Wavelet transform of image, and plot approximation and details
titles = ['Approximation', ' Horizontal detail', 'Vertical detail', 'Diagonal detail']

cA, (cH, cV, cD) = pt.dwt2(img, 'db1')
# '''
# pywt.dwt2 return structure
#                             -------------------
#                             |        |        |
#                             | cA(LL) | cH(LH) |
#                             |        |        |
# (cA, (cH, cV, cD))  <--->   -------------------
#                             |        |        |
#                             | cV(HL) | cD(HH) |
#                             |        |        |
#                             -------------------
# '''

# 顯示原圖
plt.figure()
plt.title('Original')
plt.imshow(img, 'gray')

# 顯示邊緣偵測圖片並儲存
plt.figure()
plt.title('Canny image')
plt.imshow(canny, 'gray')
pic = io.imsave('image/result/Canny image.jpg',canny)
# 顯示二值化圖片並儲存
plt.figure()
plt.title('Binarized image')
plt.imshow(thresh1, 'gray')
pic = io.imsave('image/result/Binarized image.jpg',thresh1)
# plt.imshow(dst, 'gray')
# pic = io.imsave('image/result/Binarized image2.jpg',dst)

# 顯示閉運算圖片並儲存
plt.figure()
plt.title('closing image')
plt.imshow(closing, 'gray')
pic = io.imsave('image/result/closing.jpg',closing)

# 顯示膨脹後的圖片並儲存
plt.figure()
plt.title('dilated image')
plt.imshow(dilated, 'gray')
pic = io.imsave('image/result/dilated.jpg',dilated)

# 顯示四張小波轉換後的結果圖
fig, ax = plt.subplots(2, 2, figsize=(7, 7))
fig.suptitle('1st level wavelet decomposition')
ax[0,0].set_title(titles[0])
ax[0,0].imshow(cA, 'gray')
ax[0,1].set_title(titles[1])
ax[0,1].imshow(cH, 'gray')
ax[1,0].set_title(titles[2])
ax[1,0].imshow(cV, 'gray')
ax[1,1].set_title(titles[3])
ax[1,1].imshow(cD, 'gray')

# 儲存HH Band的圖片
pic = io.imsave('image/result/HH Band.jpg',cD)

# 使用opencv套件讀取HH Band的圖片進行中值濾波
img = cv2.imread('image/result/Canny image.jpg')
img_medianBlur=cv2.medianBlur(img,5)

# 顯示中值濾波的結果並儲存
plt.figure()
plt.title('medianBlur')
plt.imshow(img_medianBlur, 'gray')
pic = io.imsave('image/result/medianBlur.jpg',img_medianBlur)

# 顯示圖片
plt.show()