# 自動紋理拼接


## 環境需求
* Python 3 (64位元)

## 安裝虛擬環境、套件
安裝 python 虛擬環境套件
```
$ pip install virtualenv
```

Clone專案，並進入專案資料夾內，創建名叫「venv」的虛擬環境(可以自行命名)。
```
$ cd WaveletTransform
$ virtualenv venv
```

啟動虛擬環境
```
$ cd venv/Scripts
$ activate
```

cd 回去上上層，安裝套件
```
$ cd ..
$ cd ..
$ pip install -r requirements.txt
```

## 參數設定
```
basemap_path  : 底圖路徑
picture_path  : 圖片路徑
savefile_path : 結果圖路徑
threshold_1   : 單邊誤差的閾值
threshold_2   : 雙邊誤差的閾值
image_width   : 結果圖的長
image_height  : 結果圖的寬
depth : 重疊的 Pixel 數
```

## 執行專案
```
$ python main.py
```