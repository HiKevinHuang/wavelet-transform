import cv2
import numpy as np
import matplotlib.pyplot as plt

img01 = cv2.imread("./image/result/HH Band.jpg")#读取目标图片


#中值滤波
img_medianBlur=cv2.medianBlur(img01,5)


# plt.imshow(img_medianBlur)
# plt.show()

cv2.imshow('medianBlur',img_medianBlur)
cv2.imwrite('./image/result/medianBlur.jpg',img_medianBlur)
cv2.waitKey(0)
cv2.destroyAllWindows()