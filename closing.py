import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage.data import data_dir
from skimage.util import img_as_ubyte
from skimage.morphology import dilation, erosion, closing
from skimage.morphology import disk, rectangle, diamond, star
from skimage import io

# 讀取圖片成灰度圖
img = cv2.imread('image/6-1/2.jpg',0)

# 做Canny邊緣偵測
canny = cv2.Canny(img, 50, 150)


# 扁平盤狀
# selem = disk(2)

# 扁平矩形形狀
# selem = rectangle(5,5)

# 菱形形狀
# selem = diamond(2)

# 星型形狀
# selem = star(1)

selem = cv2.getStructuringElement(cv2.MORPH_CROSS,(5,5))

print(selem)

close = closing(canny, selem)

pic = io.imsave('image/result/closing.jpg',close)
cv2.imshow('medianBlur',close)
cv2.waitKey(0)
cv2.destroyAllWindows()