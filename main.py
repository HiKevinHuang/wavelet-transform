import numpy as np
from makeGIF import *
from skimage import io, color
from os import walk
import time
import cv2


# 圖片路徑
savefile_path = "./Result/07/"
picture_path = "./image/圖紋計畫image/5/"
basemap_path = "./image/Basemap.jpg"
# 調整看要重疊多少pixel
depth = 10
# 閾值
threshold_1 = 15
threshold_2 = 125.5
# 結果圖的長、寬
image_width = 5
image_height = 5



# 創建底圖(1181x1181)
img = cv2.imread(basemap_path)
Basemap=cv2.hconcat([img/255] * image_width)
Basemap=cv2.vconcat([Basemap] * image_height)


record_Horizontal = []
record_vertical = []
my_arr = []


NowImg = 0   # 現在固定的圖片
NextImg = 0  # 切換下一張圖
mode = 0     # 更改模式(1:左右鏡像  2:上下鏡像  3:上下左右鏡像)
count = 0
start_width = 0
start_height = 0


# 列出資料夾中的所有圖片
def HowManyPictures():
    if not os.path.exists(picture_path):
        print("請檢查路徑 !")
    else:
        for root, dirs, files in walk(picture_path):
            files = sorted(files)
    return files, picture_path
        
files, mypath = HowManyPictures()


# 存放鏡像後的圖片
LRMirrorImage = []
UDMirrorImage = []
LRUDMirrorImage = []
# 放置拼接好的圖
target = []
# 動態產生變數且一起讀圖進來
AllImage = []


def ReadImage():
    Mylist = []
    for index in range(len(files)):
        locals()['original_img'+str(index)] = io.imread(mypath + files[index])
    Mylist.append(locals())
    del Mylist[0]['index']
    del Mylist[0]['Mylist']
    for key, value in Mylist[0].items():
        AllImage.append(value)
    global target
    target = AllImage


merge_target_Horizontal = ""
merge_target_vertical = ""


# 橫向拼接
def SidebySide_Horizontal():
    global NowImg, NextImg, depth, target, mode
    print("目前固定第幾張 : " + str(NowImg))
    print("目前跟誰拼 : " + str(NextImg))
    print("目前模式 : " + str(mode))
    img1_height, img1_width, a1 = target[NowImg].shape
    img2_height, img2_width, a2 = target[NextImg].shape

    # 轉成LAB空間
    img1 = color.rgb2lab(target[NowImg])
    img2 = color.rgb2lab(target[NextImg])

#    print("目前縫合點為: " + str(depth))

    # 找要切割的範圍
    L1 = img1[:,img1_width-depth:,0]
    L2 = img2[:,:depth,0]
    A1 = img1[:,img1_width-depth:,1]
    A2 = img2[:,:depth,1]
    B1 = img1[:,img1_width-depth:,2]
    B2 = img2[:,:depth,2]
    # 兩張照片LAB值相減
    L = L1 - L2
    A = A1 - A2
    B = B1 - B2
    # 兩張圖片LAB值相減後做最小平方法
    value = np.sqrt(np.square(L) + np.square(A) + np.square(B))
    # 切割後的圖片長寬大小
    height, width = value.shape
    # 切割後的圖片有幾個pixel
    pixel_sum = height * width
    # 計算平均值
    avg = value.sum() / pixel_sum

    print("橫向LAB誤差平均值 : " + str(avg))

    if avg >= threshold_1:
        depth = depth - 1
        if depth == 0:
            depth = 10
            NextImg += 1
            if NextImg < len(target):
                print("---------------------下一張----------------------")
                SidebySide_Horizontal()
            else:
#                time.sleep(1)
                NextImg = 0
                if mode == 0:
                    target = LRMirrorImage
                    mode = 1
                elif mode == 1:
                    target = UDMirrorImage
                    mode = 2
                elif mode == 2:
                    target = LRUDMirrorImage
                    mode = 3
                else:
                    print("沒有符合的圖片了，請調整閾值後再試一次")
                print("沒有符合的圖片了，已切換到模式 " + str(mode))
                time.sleep(5)
                SidebySide_Horizontal()
                
        else:
            print("----------------------重算-----------------------")
            print(" ")
            SidebySide_Horizontal()
    else:
        NowImg = NextImg
        merge_point = img1_width - depth
        record_Horizontal.append(["右邊",NextImg,depth,mode])
        print(record_Horizontal)
#        time.sleep(5)
        target = AllImage
        return merge_point
    
    
# 直向拼接
def SidebySide_Vertical():
    global NowImg, NextImg, depth, target, mode
    print("目前固定第幾張 : " + str(NowImg))
    print("目前跟誰拼 : " + str(NextImg))
    print("目前模式 : " + str(mode))
    img1_height, img1_width, a1 = target[NowImg].shape
    img2_height, img2_width, a2 = target[NextImg].shape

    # 轉成LAB空間
    img1 = color.rgb2lab(target[NowImg])
    img2 = color.rgb2lab(target[NextImg])

#    print("目前縫合點為: " + str(depth))

    # 找要切割的範圍
    L1 = img1[img1_height-depth:,:,0]
    L2 = img2[:depth,:,0]
    A1 = img1[img1_height-depth:,:,1]
    A2 = img2[:depth,:,1]
    B1 = img1[img1_height-depth:,:,2]
    B2 = img2[:depth,:,2]
    # 兩張照片LAB值相減
    L = L1 - L2
    A = A1 - A2
    B = B1 - B2
    # 兩張圖片LAB值相減後做最小平方法
    value = np.sqrt(np.square(L) + np.square(A) + np.square(B))
    # 切割後的圖片長寬大小
    height, width = value.shape
    # 切割後的圖片有幾個pixel
    pixel_sum = height * width
    # 計算平均值
    avg = value.sum() / pixel_sum

    print("直向LAB誤差平均值 : " + str(avg))

    if avg >= threshold_1:
        depth = depth - 1
        if depth == 0:
            depth = 10
            NextImg += 1
            if NextImg < len(target):
                print("---------------------下一張----------------------")
                SidebySide_Vertical()
            else:
#                time.sleep(1)
                NextImg = 0
                if mode == 0:
                    target = LRMirrorImage
                    mode = 1
                elif mode == 1:
                    target = UDMirrorImage
                    mode = 2
                elif mode == 2:
                    target = LRUDMirrorImage
                    mode = 3
                else:
                    print("沒有符合的圖片了，請調整閾值後再試一次")
                print("沒有符合的圖片了，已切換到模式 " + str(mode))
                time.sleep(5)
                SidebySide_Vertical()
                
        else:
            print("----------------------重算-----------------------")
            print(" ")
            SidebySide_Vertical()
    else:
        NowImg = NextImg
        merge_point = img1_height - depth
        record_vertical.append(["下面",NextImg,depth,mode])
        print(record_vertical)
#        time.sleep(5)
        target = AllImage
        return merge_point
    

# 計算橫向及直向的最小平方
def SidebySide3():
    global NowImg, NextImg, depth, Basemap, start_width, start_height, count

    imageA_height, imageA_width, a1 = AllImage[0].shape
    ImageB_height, ImageB_width, a1 = AllImage[NextImg].shape
    
    # 找出每一張要比對的起始點高、寬
    start_width_local = start_width + imageA_width - depth
    start_height_local = start_height + imageA_height - depth
    
    # ㄏ字型區域
    imageA = Basemap[start_height_local:start_height_local+ImageB_height, start_width_local:start_width_local+ImageB_width, :]
    # 輸入圖的ㄏ字型區域
    image_copy = AllImage[NextImg].copy()
    # 輸入圖的ㄏ字型以外都先變黑色
    image_copy[depth:, depth:, :] = 0
    
    # 轉成LAB空間
    img1 = color.rgb2lab(imageA)
    img2 = color.rgb2lab(image_copy)
    
    # 兩張照片LAB值相減
    L = img1[:,:,0]-img2[:,:,0]
    A = img1[:,:,1]-img2[:,:,1]
    B = img1[:,:,2]-img2[:,:,2]
    # 兩張圖片LAB值相減後做最小平方法
    value = np.sqrt(np.square(L) + np.square(A) + np.square(B))
    # 切割後的圖片長寬大小
    height, width = value.shape
    # 切割後的圖片有幾個pixel
    pixel_sum = height * width
    # 計算平均值
    avg = value.sum() / pixel_sum
    print("平均: ", avg)
    my_arr.append([NextImg, depth, avg, start_width_local, start_height_local])
    
    if len(my_arr) < len(AllImage) * 10:
        if avg >= threshold_2:
            print("第 " + str(NextImg) + " 張")
            print("深度: " + str(depth))
            depth = depth - 1
            if depth == 0:
                depth = 10
                NextImg += 1
                if NextImg < len(AllImage):
                    SidebySide3()
                else:
                    NextImg = 0
                    SidebySide3()
            else:
                SidebySide3()
        else:
            Basemap[start_height_local:start_height_local+ImageB_height, start_width_local:start_width_local+ImageB_width, :] = AllImage[NextImg][:,:,[2,1,0]]
            cv2.imwrite(savefile_path + "out" + str(count) + '.jpg', Basemap, [cv2.IMWRITE_JPEG_QUALITY, 100])
            count += 1
            depth = 10
            start_width = start_width_local
    else:
        find_avg_min = []
        # 若毫無結果，就找出所有圖的色差中最小的那張圖還有深度
        for i in range(len(my_arr)):
            find_avg_min.append(my_arr[2])
        best_result = my_arr[find_avg_min.index(min(find_avg_min))][0]
        # best_result_start_width_local = my_arr[find_avg_min.index(min(find_avg_min))][3]
        # best_result_start_height_local = my_arr[find_avg_min.index(min(find_avg_min))][4]
        Basemap[start_height_local:start_height_local+ImageB_height, start_width_local:start_width_local+ImageB_width, :] = AllImage[best_result][:,:,[2,1,0]]
        cv2.imwrite(savefile_path + "out" + str(count) + '.jpg', Basemap, [cv2.IMWRITE_JPEG_QUALITY, 100])
        count += 1
        depth = 10
        start_width = start_width_local
        my_arr.clear()
    return start_height_local


# 左右翻轉
def lrmirror():
    for index in range(len(AllImage)):
        LRMirrorImage.append(np.fliplr(AllImage[index]))
    

# 上下翻轉
def udmirror():
    for index in range(len(AllImage)):
        UDMirrorImage.append(np.flipud(AllImage[index]))
        

# 上下左右翻轉
def lrudmirror():
    for index in range(len(AllImage)):
        LRUDMirrorImage.append(np.flipud(LRMirrorImage[index]))


# 更改拼接方法(橫向拼接)
def merge_Horizontal():
    global merge_target_Horizontal, count
    
    for index in range(len(record_Horizontal)):
        imageA = merge_target_Horizontal
        if record_Horizontal[index][3] == 0:
            imageB = AllImage[record_Horizontal[index][1]]
        elif record_Horizontal[index][3] == 1:
            imageB = LRMirrorImage[record_Horizontal[index][1]]
        elif record_Horizontal[index][3] == 2:
            imageB = UDMirrorImage[record_Horizontal[index][1]]
        imageA_width = imageA.shape[1]
        
        # 兩張圖片的連接處
        merge_point_Horizontal = imageA_width - record_Horizontal[index][2]
        # 合併圖片
        merge_target_Horizontal = np.hstack((imageA[:,:merge_point_Horizontal,:],imageB))

    merge_target_Horizontal[:,:,:] = merge_target_Horizontal[:,:,[2,1,0]]
    # cv2.imwrite(savefile_path + "Horizontal.jpg", merge_target_Horizontal, [cv2.IMWRITE_JPEG_QUALITY, 100])
    print("Horizontal success !!!!")
    
    
# 更改拼接方法(直向拼接)
def merge_Vertical():
    global merge_target_vertical, count
    for index in range(len(record_vertical)):
        imageA = merge_target_vertical
        if record_Horizontal[index][3] == 0:
            imageB = AllImage[record_vertical[index][1]]
        elif record_Horizontal[index][3] == 1:
            imageB = LRMirrorImage[record_vertical[index][1]]
        elif record_Horizontal[index][3] == 2:
            imageB = UDMirrorImage[record_vertical[index][1]]
        imageA_height = imageA.shape[0]
        # 兩張圖片的連接處
        merge_point = imageA_height - record_vertical[index][2]
        merge_target_vertical = np.vstack((imageA[:merge_point,:,:],imageB))

    merge_target_vertical[:,:,:] = merge_target_vertical[:,:,[2,1,0]]
    # cv2.imwrite(savefile_path + "Vertical.jpg", merge_target_vertical, [cv2.IMWRITE_JPEG_QUALITY, 100])
    print("Vertical success !!!!")


# 直向加橫向拼接
def merge4():
    global merge_target_Horizontal, merge_target_vertical, Basemap, count
    Horizontal_height = merge_target_Horizontal.shape[0]
    Horizontal_width = merge_target_Horizontal.shape[1]
    vertical_height = merge_target_vertical.shape[0]
    vertical_width = merge_target_vertical.shape[1]
    Basemap[:Horizontal_height,:Horizontal_width,:] = merge_target_Horizontal
    Basemap[:vertical_height,:vertical_width,:] = merge_target_vertical
    # cv2.imwrite(savefile_path + "L.jpg", Basemap, [cv2.IMWRITE_JPEG_QUALITY, 100])


# 檢查資料夾是否存在
def checkIfDirectoryExists():
    if not os.path.exists(savefile_path):
        os.makedirs(savefile_path)


# 儲存參數
def saveParams():
    #write
    text_file = open(savefile_path + 'params.txt', "w")
    text_file.write("image_width: %d \nimage_height: %d \nthreshold_1: %.2f \nthreshold_2: %.2f \ndepth:  %d" % (image_width, image_height, threshold_1, threshold_2, depth))
    text_file.close()


def main():
    global merge_target_vertical, merge_target_Horizontal, start_height, start_width
    checkIfDirectoryExists()
    saveParams()
    ReadImage()
    merge_target_Horizontal = AllImage[0]
    merge_target_vertical = AllImage[0]
    lrmirror()
    udmirror()
    lrudmirror()
    for x in range(image_width):
        print("迴圈次數 : " + str(x))
        SidebySide_Horizontal()
    print("-----------迴圈結束-----------")
    for y in range(image_height):
        print("迴圈次數 : " + str(x))
        SidebySide_Vertical()
    print("-----------迴圈結束-----------")
    merge_Horizontal()
    merge_Vertical()
    merge4()
    for y in range(image_height):
        for x in range(image_width):
            print("執行中間區域拼接，目前第" + str(y) + "行")
            print("X 迴圈次數 : " + str(x))
            if x == 0:
                print("X 的第0次迴圈")
                start_height_local = SidebySide3()
                newline = start_height_local
            else:
                SidebySide3()  # 偶爾會出問題(待修)
        start_height = newline
        start_width = 0
        print("-----------迴圈結束-----------")
        time.sleep(10)

    # save to gif 
    outputPath = savefile_path + "outGif.gif"
    makeGif(savefile_path, outputPath, frame_every_X_steps = 1, repeat_ending = 5)

if __name__ == '__main__':
    main()
