# -*-coding:utf-8-*-
# opencv-contrib-python 版本 3.4.2.16
# opencv 版本 3.4.2.16
import cv2
import numpy as np
# print(cv2.__version__)

# 轉灰階
# gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

def main():
    # 圖片路徑
    img_route = 'image/result/Binarized image2.jpg'

    # 讀圖
    img = cv2.imread(img_route)

    # DrawKeyPoint(img)
    DrawKeyPoint(img)

def KpsAndDes(img):
    # 設定黑賽矩陣(Hessian)閾值為400 (使用SIFT演算法)
    # sift=cv2.xfeatures2d.SIFT_create(400)

    # 設定黑賽矩陣(Hessian)閾值為400 (使用SURF演算法)
    surf = cv2.xfeatures2d.SURF_create(400)

    # 檢查當前閾值
    # print(surf.getHessianThreshold())

    # 查找特徵點 & 描述符
    kps, des = surf.detectAndCompute(img,None)

    return kps, des

def DrawKeyPoint(img):
    kps, des = KpsAndDes(img)

    # 畫出特徵點
    img = cv2.drawKeypoints(img,kps,img)

    SaveImage(img)
    ShowImage(img)

def ShowImage(img):
    cv2.imshow('surf_keypoints',img)
    cv2.waitKey()
    cv2.destroyAllWindows()

def SaveImage(img):
    # 儲存圖片
    cv2.imwrite('image/result/surf_keypoints2.jpg',img)

if __name__ == '__main__':
    main()