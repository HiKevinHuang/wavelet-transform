#coding: utf-8
import numpy as np
import cv2
 
leftgray = cv2.imread('image/result/Binarized image.jpg')
leftgray = cv2.resize(leftgray, (640, 480))
rightgray = cv2.imread('image/result/Binarized image2.jpg')
rightgray = cv2.resize(rightgray, (640, 480))
 
hessian=400
surf=cv2.cv2.xfeatures2d.SURF_create(hessian) #将Hessian Threshold设置为400,阈值越大能检测的特征就越少
kp1,des1=surf.detectAndCompute(leftgray,None)  #查找关键点和描述符
kp2,des2=surf.detectAndCompute(rightgray,None)
 
 
FLANN_INDEX_KDTREE=0   #建立FLANN匹配器的参数
indexParams=dict(algorithm=FLANN_INDEX_KDTREE,trees=5) #配置索引，密度树的数量为5
searchParams=dict(checks=50)    #指定递归次数
#FlannBasedMatcher：是目前最快的特征匹配算法（最近邻搜索）
flann=cv2.FlannBasedMatcher(indexParams,searchParams)  #建立匹配器
matches=flann.knnMatch(des1,des2,k=2)  #得出匹配的关键点

good=[]
#提取优秀的特征点
for m,n in matches:
    if m.distance < 0.5*n.distance: #如果第一个邻近距离比第二个邻近距离的0.7倍小，则保留
        good.append(m)
 
draw_params = dict(matchColor = (0,255,0),singlePointColor = None, flags = 2) # draw matches in green color

img3 = cv2.drawMatches(leftgray,kp1,rightgray,kp2,good,None,**draw_params)
cv2.imshow("original_image_drawMatches.jpg", img3)


cv2.waitKey()
cv2.destroyAllWindows()
